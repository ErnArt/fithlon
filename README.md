![# Fithlon](resources/logoFithlon.png "Logo Fithlon")

# Projet d'Agilité en Master Web.


[[_TOC_]]

# Description du projet

Notre projet consiste à créer un nouveau service pour gérer des indicateurs de santé. En effet,
nous devons créer un service qui permet de gérer l’ensemble des indicateurs de santé (poids, IMC, sommeil, fréquence cardiaque, ...). L'utilisateur pourra donc visualiser ses propres statistiques.

# Fonctionnalités disponible 
  - L'utilisateur peut calculer son IMC
  - L'utilisateur peut s'inscrire 
  - L'utilisateur peut se connecter une fois inscrit 
  - L'utilisateur peut visualiser l'évolution de son IMC chaque jour

Dans de prochaine mise à jours, le site web permettrai de suivre son évolution de pas ainsi que sa quantité de sommeil et d'obtenir des informations et des conseils en fonction du besoin de chacun.

----

# Lancement du projet via Heroku 

http://fithlon.herokuapp.com/

# Lancement depuis un serveur local 

## Lancement du projet via GitLab

```
git clone https://gitlab.com/ErnArt/fithlon.git
cd fithlon
npm install
npm install nodemon --save-dev
npm install better-sqlite3 --save
npm install bcrypt --save
DEBUG=fithlon:* npm run devstart
```


## Implementation de cypress :

Permet d’installer cypress
Attention, cette méthode semble installer cypress sur la machine, et non dans le projet.

```
npx cypress install --force
```

Lancement de cypress (interface web)
```
npx cypress open
```

Lancement de cypress (tests effectués dans la console)
```
npx cypress run
```

# Définition de "Done" 

Notre projet est organisé de la manière suivante : 

Nous avons plusieurs milestones, qui sont comme les grosses étapes de notre projet. 
Chacune de ces milestones sont composées d'issues, que l'on classe sur un tableau (board) en fonction de si le problème est résolu, en cours de résolution, ou urgent. 
Pour nous, "Done" signifie donc la résolution des milestones necessaire à l'avancement du projet 


<br>

# Liste de tâches 

  - [x] Algorithme IMC
  - [x] Implémentation de Cypress
  - [x] Création page d'inscription
  - [x] Design du logo Fithlon
  - [x] Création de la base de données
  - [x] Stockage sécurisé dans la base de données
  - [x] Enregistrement des indicateurs de santé dans la base de données
  - [x] Connexion de l'utilisateur
  - [x] Intégration Heroku CICD 
  - [x] Différents tests fonctionnels avec Cypress
  - [x] Documentation des différentes fonctionnalités du programme
  - [x] Implémentation ESlint
  - [x] Tests avec ESlint
  - [x] Documentation technique
  - [x] Graphique d'évolution de l'IMC / Poids

----

# Information d'utilisation pour l'utilisateur 

### Une fois connecté sur le site Fithlon, la page d'acceuil se présente de cette manière :

![# Page d'accueil](resources/homePage.png "Page d'accueil")

(*) Logo du site : En cliquant dessus, n'importe quand, on revient ici, sur la page d'accueil

(1) Bouton "Compte", permet de se connecter ou de s'inscrire 

(2) Champs permettant de saisir le poids et la taille

(3) Une fois les deux champs complétés, cliquer sur le bouton "Calculer votre IMC" permet de calculer l'IMC

(4) Le résultat du calcul de l'IMC s''affichera ici, avec l'IMC obtenue et sa signification 

<br>

### Interface obtenue en cliquant sur le bouton "Compte" :

![# Modal de connexion](resources/connexionModal.png "Modal de connexion")

(1) Champs permettant de saisir l'identifiant (adresse e-mail) ainsi que le mot de passe pour se connecter au compte utilisateur

(2) Une fois les deux champs complétés, cliquer sur le bouton "Connexion" afin de se connecter au compte utilisateur : un message annoncera que l'utilisateur est bien connecté et il pourra bien voir son nom
Sinon, afin de se créer un compte, cliquer directement sur le bouton "Inscription" afin d'accèder au formulaire d'inscription

(3) Annuler et retourner à la première page d'acceuil

<br>

### Interface obtenue en cliquant sur le bouton "Inscription" depuis l'interface précédente:

![# Inscription](resources/inscription.png "Inscription")

(1) Barre d'avancement de l'inscription 

(2) Champs à compléter pour le nom et le prénom

(3) Champs à compléter  pour l'adresse mail qui doit avoir un format valide

(4) Champs à compléter pour le mot de passe et confirmation du mot de passe
Note : Il doit avoir minimum 6 caractères, et il y a un indicateur qui permet de connaître le degré de sécurité du mot de passe 

(5) Case à cocher pour confirmer les informations saisies 

(6) Une fois les champs complétés, cliquer sur les bouton "Inscription" une fois l'ensemble des champs complétés, afin de finaliser l'inscription
Note : Si l'inscription est valide (tout les champs complétés et mot de passe respectant les conditions), le site amène l'utilisateur à la page principale (et il peut donc maintenant se connecter en cliquant de nouveau sur le bouton "Compte" et en saisissant ses informations)

### Interface obtenue une fois que l'utilisateur est connecté :

![# Historique](resources/historique.png "Historique")

(*) Si l'utilisateur est connecté, le bouton "Compte" est remplacé par les boutons "Historique" et "Se déconnecter"
-> Par exemple, si l'utilisateur à cliqué sur le bouton "Historique", il pourra visualiser son évolution de son IMC, mais pour de nouveau entrer un nouveau calcul d'IMC, il devra cliquer sur le logo afin de revenir à la page d'acceuil permettant d'effectuer se calcul, comme dit dans les exemples de pages précédentes

(1) Bouton pour accéder à l'historique et d'afficher le graphique d'évolution de l'IMC

(2) Bouton permettant à l'utilisateur de se déconnecter, et donc de terminer sa session

(3) Exemple de graphique de suivi de l'IMC d'une personne en fonction de l'IMC et de la date à laquelle il l'a calculé

<br>
   
 # Et suivre l'évolution afin de voir les prochaines fonctionnalités
  - [ ] Permettre la saisie des données : nombre de pas, durée du sommeil, fréquence cardiaque
  - [ ] Compteur de pas
  - [ ] Compteur de quantité de sommeil 
  - [ ] Gestion de la fréquence cardiaque

<br>

# Auteurs du projet

  - Artigas Ernesto
  - Devilliers Matthias
  - Fovet Alexandre
  - Ringot Arthur
  - Telliez Alexis