![# Fithlon](resources/logoFithlon.png "Logo Fithlon")

# Projet d'Agilité en Master Web.

[[_TOC_]]

Ce fichier permet de regrouper et de détailler l'ensemble des fonctions utilisées dans notre site, sous la forme suivante : 

    Nom du fichier 
    * Nom d'une fonction(Paramètres)
        - Description
        - Ce qui est renvoyé
    * Nom d'une fonction
        - ... 
         
# dossier : public/javascripts
## calculIMC.js 
* calculIMC(poids, taille)
    - Permet de calculer l'IMC à partir du poids et de la taille 
    - Retourne un IMC 
* classificationIMC(imc)
    - Permet de classifier l'IMC dans une catégories
    - Retourne une catégorie

## checker.js 
* lastname = function()
    - Récupère le nom dans le formulaire et regarde si il est supérieur ou égal à 2
    - Retourne un boolean
* firstname = function()
    - Récupère le prénom dans le formulaire et regarde si il est supérieur ou égal à 2
    - Retourne un boolean 
* email2 = function()
    - Récupère l'email dans le formulaire et regarde si il correspond bien à la forme example42@exemple.domaine
    - Retourne un boolean
* password2 = function()
    - Récupère le mot de passe dans le formulaire et verifie qu'il est bien supérieur à 6 caractères 
    - Retourne un boolean
* password3 = function()
    - Récupère le mot de passe et le mot de passe de confirmation pour savoir si ils sont identique 
    - Retourne un boolean
* defaultCheck1 = function()
    - Regarde si la case à cocher et si elle est bien cochée
    - Retourne un boolean
* passStrength()
    - Regarde la complexité du mot de passe :
		- si il n'est pas correcte
		- si il est bien supérieur à 6
		- si il utilise bien des lettres, des chiffres ou caractères spéciaux 
    - Retourne un entier en fonction de la complexité

## feedback.js 
* validate(target)
    - Permet de définir un champ comme valide 
* invalidae(target)
    - Permet de définir un champ comme non remplit
* error(targer)
    - Permet de définir un champ comme invalide / erroné 
* document.querySelectorAll("#sign").forEach ( => ...)
    - Ici, on regarde le nombre d'élément valide afin de faire progresser la barre de progression en haut du formulaire d'inscription

## sign.js
* document.querySelectorAll("input, select").forEach ( => ...)
    - Checker général de la force du mot de passe du Feedback
* document.querySelector("#password2").onkeyup = function(event)
    - règle l'affichage de la force du mot de passe en fonction du degré de sécurité de celui-ci (il y a 3 niveaux de sécurité, mais le niveau 1 est condiré comme un mot de passe erroné, donc on pourrait dire qu'il y a que 2 niveaux au final)
* document.querySelector("#sign").onsubmit = function(event) 
    - On vérifie si tout les champs remplit sont valides : si oui, le fomulaire et confirmé et l'inscription est finalisée, sinon on indique les champs non valides

<br>

# dossier : models
## dataManager.js
* insertData = async function(idUser, weight, height, bmi) 
    - Se connecte à la base de données, envoie une requête d'insertion à la base de données et ferme la base de données
* getAllDataFromUser = async function(idUser) 
	- Effectue une requête qui récupère toutes les données de l'utilisateur
	- Retourne un résultat de la requête
* getLastDataFromUser = async function(idUser) 
	- Effectue une requête qui récupère toutes les données de l'utilisateur
	- Retourne un résultat de la requête
* function execRequest(sqlU, idUser)
	- sqlU est une requête
	- Execute la requête
	- Retourne le résultat de la requête

## manager.js
* exports.connect = ()
	- Effectue la connexion avec la base de données

## userManager.js
* insertUser = function(lastname, firstname, email, passwd)
	- Prend en paramètre le nom, le prénom, l'email et le mot de passe
	- Crypte le mot de passe et envoie une requête d'insertion dans la base de données
* getUser = function(email, passwd)
	- Effectue une requête et regarde si il est bien dans la base de données et que identifiants sont correctes
* verifyMail = function(email)
	- Effectue une requête et regarde si l'email n'est pas déjà dans la base de données

<br>

# dossier : controllers
## indexController.js 
* index = function(req, res)
    - Renvoie à la vue de la page d'acceuil du site
	
## userController.js 
* signUp = function(req, res)
    - Renvoie à la vue de la page d'inscription du site
* signIn = function(req, res)
    - Quand on a cliqué sur le bouton "Connexion" depuis le modal du bouton "Compte", vérifie si la connection est accordée, en vérifiant les mots de passes et les noms d'utilisateurs grâce à une requete sql
* register = function(req, res)
    - Quand on a cliqué sur le bouton "Inscription" depuis le formulaire d'inscription, vérifie si le mail n'existe pas déjà, sinon ajoute l'utilisateur à la base de donnée et valide l'inscription
* signOut = function(req, res)
    - Quand on a cliqué sur "Se déconnecter", termine la session de l'utilisateur

## historiqueController.js 
* historique = async function(req, res)
    - Permet de voir l'historique de la personne connectée 
	
## dataController.js 
* exports.sendData = function(req, res)
	- Si l'utilisateur existe dans la base de donnée, on ajoute les données qu'il vient de rentrer dans la base donnée, sinon on ne fait rien s'il n'y a aucun ocmpte associé (Evite les potentiels bugs de base de donnée)

<br>

# Information sur les bases de données

## Structures

### Users 
- id -> ID de l'utilisateur
- lastname -> Nom de l'utilisateur
- firstname -> Prénom de l'utilisateur
- email -> E-mail de l'utilisateur
- passwd -> Mot de passe de l'utilisateur 

### Data
- id -> ID de la donnée
- idUser -> Id de l'utilisateur en question
- weight -> Poids
- height -> Taille 
- bmi -> Imc 

# Auteurs du projet

  - Artigas Ernesto
  - Devilliers Matthias
  - Fovet Alexandre
  - Ringot Arthur
  - Telliez Alexis