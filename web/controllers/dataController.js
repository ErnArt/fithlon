const dataManager = require("../models/dataManager");

exports.sendData = function(req)
{
	let idUser = req.session.idUser;
	let poids = req.body.poids;
	let taille = req.body.taille;
	let imc = req.body.imc;

	if(idUser != undefined){
		dataManager.insertData(idUser, poids, taille, imc);
	}
	else{
		console.log("Aucun compte associé.");
	}
};