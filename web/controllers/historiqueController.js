const dataManager = require("../models/dataManager");

exports.historique = async function(req, res)
{
	let allDatas = await dataManager.getAllDataFromUser(req.session.idUser);
	res.render("../views/layoutPage.ejs", {title: "Historique", page: "history", allDatas: allDatas});
};