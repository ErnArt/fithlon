// User page

const manager = require("../models/userManager");

exports.signUp = function(req, res)
{
	res.render("../views/layoutPage.ejs", {title : "Inscription", page : "inscription",
		emailVar : req.query.status
	});
};

exports.signIn = function(req, res)
{
	let resSQL = manager.getUser(req.body.email, req.body.password);
	// Si les mots de passes ne concordaient pas on aura un mot de passe vide
	if (resSQL[0] == undefined || resSQL[0].passwd.length == 0) {
		res.redirect("/?status=error");
	}

	else {
		req.session.name = resSQL[0].firstname;
		req.session.idUser = resSQL[0].id;
		res.redirect("/?status=success");
	}
};

exports.register = function(req, res)
{
	let lastname = req.body.lastname;
	let firstname = req.body.firstname;
	let email = req.body.email2;
	let password = req.body.password2;

	// On vérifie d"abord si le mail n"existe pas déjà dans la base de données
	let verifyMail = manager.verifyMail(email);

	// Si le mail est déjà dans la base de données
	if (Object.keys(verifyMail).length != 0)
	{
		res.redirect("/inscription?status=alreadyDB");
	}

	// Sinon, on rajoute dans la base de données l"utilisateur
	else
	{
		// On lui donne les requêtes
		manager.insertUser(lastname, firstname, email, password);
		res.redirect("/inscription?status=inscriptionWorked");
	}
};

exports.signOut = function(req, res)
{
	req.session.destroy();
	// On doit aller dans productsController pour s"occuper de récupérer status
	res.redirect("/?status=logout");
};