/* global describe it cy */

describe("Home page", () => {
	it("main", () => {
		cy.visit("http://localhost:3000/");
		cy.get("form").then(form$ => {
			form$.on("submit", e => {
				e.preventDefault();
			});
		});
	});
});

describe("Vérification du formulaire", () => {
	it("Poids", () => {
		cy.get("#poids").clear().type(60);
		cy.get("#poids").should("have.value",60);
	});
	
	it("Taille", () => {
		cy.get("#taille").clear().type(180);
		cy.get("#taille").should("have.value",180);
	});

	it("Calcul", () => {
		cy.get("#boutonIMC").click();
		cy.get("#valeurIMC").contains(18.52);
		cy.get("#categorieIMC").contains("Corpulence normale.");
	});
});

describe("Type de retour - IMC", () => {
	it("Calcul - type 1", () => {
		cy.get("#poids").clear().type(30);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Maigreur sévère.");
	});

	it("Calcul - type 2", () => {
		cy.get("#poids").clear().type(42);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Maigreur.");
	});

	it("Calcul - type 3", () => {
		cy.get("#poids").clear().type(45);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Léger sous-poids.");
	});
	
	it("Calcul - type 4", () => {
		cy.get("#poids").clear().type(50);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Corpulence normale.");
	});
	
	it("Calcul - type 5", () => {
		cy.get("#poids").clear().type(70);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Surpoids.");
	});

	it("Calcul - type 6", () => {
		cy.get("#poids").clear().type(80);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Obésité modérée.");
	});

	it("Calcul - type 7", () => {
		cy.get("#poids").clear().type(90);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Obésité sévère.");
	});

	it("Calcul - type 8", () => {
		cy.get("#poids").clear().type(110);
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#categorieIMC").contains("Obésité massive.");
	});
});

describe("Type de retour - Erreur", () => {
	it("Calcul - erreur 1", () => {
		cy.get("#poids").clear().type(30);
		cy.get("#taille").clear().type("fdsf");
		cy.get("#boutonIMC").click();
		cy.get("#valeurIMC").contains("Erreur. Veuillez entrer un nombre.");
	});
	
	it("Calcul - erreur 2", () => {
		cy.get("#poids").clear().type("50fds");
		cy.get("#taille").clear().type(160);
		cy.get("#boutonIMC").click();
		cy.get("#valeurIMC").contains("Erreur. Veuillez entrer un nombre.");
	});
});