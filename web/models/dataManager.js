const manager = require("./manager");

exports.insertData = async function(idUser, weight, height, bmi) 
{
	// On se connecte avec la base de donnée
	const db = manager.connect();

	const sqlU = 
	`INSERT INTO data (idUser, date, weight, height, bmi) 
		VALUES (?, DATE('now'), ?, ?, ?)`;
	db.prepare(sqlU).run(idUser, weight, height, bmi);
	
	// On ferme notre base de donnée 
	db.close();
	return;
};

exports.getAllDataFromUser = async function(idUser) 
{
	const sqlU = 
	`SELECT weight, height, bmi, date
		FROM data
		WHERE idUser = ? AND 
		id IN(SELECT MAX(id) FROM data GROUP BY date);`;
	return execRequest(sqlU, idUser);
};

function execRequest(sqlU, idUser){
	const db = manager.connect();
	const resSQLU = db.prepare(sqlU).all(idUser);
	db.close();
	return resSQLU;
}