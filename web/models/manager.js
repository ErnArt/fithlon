const path = require("path");
const sqlite3 = require("better-sqlite3");
var __dirname = path.resolve();

exports.connect = () => new sqlite3
(
	path.join(__dirname, "./db/fithlon.db")
);