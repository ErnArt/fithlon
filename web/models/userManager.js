const manager = require("./manager");
const bcrypt = require("bcrypt");

exports.insertUser = function(lastname, firstname, email, passwd) 
{
	// On se connecte avec la base de donnée
	const db = manager.connect();

	// Pour chiffrer notre mot de passe, 10 pour meilleur salage
	let cryptedPasswd = bcrypt.hashSync(passwd, 10);

	// On prépare notre requête pour insérer des valeurs
	const sqlU = 
	`INSERT INTO users 
		(lastname, firstname, email, passwd) 
	VALUES (?, ?, ?, ?)`;
	
	// On met .run() pour lancer la requête avec les paramètres dedans
	db.prepare(sqlU).run(lastname, firstname, email, cryptedPasswd);
	
	// On ferme notre base de donnée 
	db.close();

};

exports.getUser = function(email, passwd)
{
	const db2 = manager.connect();

	const sqlU = 
	`SELECT id, lastname, firstname, passwd
		FROM users
		WHERE email = ?`;

	const resSQLU = db2.prepare(sqlU).all(email);

	if (resSQLU[0] != undefined && !(bcrypt.compareSync(passwd, resSQLU[0].passwd)))
	{
		// Si le mot de passe n'est pas le bon on retourne un mot de passe vide
		resSQLU[0].passwd = "";
	}
	
	db2.close();
	return resSQLU;
};

exports.verifyMail = function(email)
{
	// On vérifie si un email n'est pas associé à un id
	const db3 = manager.connect();
	const sqlMail = 
	`SELECT id
		FROM users
		WHERE email = ?`;
	
	const resSQLMail = db3.prepare(sqlMail).all(email);
	db3.close();
	return resSQLMail;
};