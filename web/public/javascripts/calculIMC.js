function calculIMC (poids, taille) {
	let imc = (poids / ((taille/100)*(taille/100))).toFixed(2);
	if (imc == Infinity)
		imc = 0;

	document.getElementById("imc").value = imc;
	return imc;
}

function classificationIMC(imc){
	if (imc < 16) {
		return "Maigreur sévère.";
	}
	
	else if (imc < 17) {
		return "Maigreur.";
	}

	else if (imc < 18.5) {
		return "Léger sous-poids.";
	}

	else if (imc < 25) {
		return "Corpulence normale.";
	}

	else if (imc < 30){
		return "Surpoids.";
	}

	else if (imc < 35) {
		return "Obésité modérée.";
	}

	else if (imc < 40) {
		return "Obésité sévère.";
	}

	else {
		return "Obésité massive.";
	}
}

var poids = document.querySelector("#poids");
var taille = document.querySelector("#taille");

var valeurIMC = document.querySelector("#valeurIMC");
var categorieIMC = document.querySelector("#categorieIMC");


const boutonIMC = document.querySelector("#boutonIMC");
boutonIMC.onclick = ( () => {
	if (isNaN(calculIMC(poids.value, taille.value))) {
		valeurIMC.innerHTML = "Erreur. Veuillez entrer un nombre.";
		categorieIMC.innerHTML = "";
	}

	else {
		valeurIMC.innerHTML = calculIMC(poids.value, taille.value);
		categorieIMC.innerHTML = classificationIMC(calculIMC(poids.value, taille.value));
	}
});