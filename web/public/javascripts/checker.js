const Checker = {};

Checker.lastname = function() 
{
	const lastname = document.querySelector("#lastname");
	return lastname.value.length >= 2;
};

Checker.firstname = function() 
{
	const firstname = document.querySelector("#firstname");
	return firstname.value.length >= 2;
};

Checker.email2 = function() 
{
	const email2 = document.querySelector("#email2").value;
	const emailRegex = /^([a-zA-Z]+|[0-9]+)\.*[a-zA-Z]*[0-9]*@[a-zA-Z]+\.[a-zA-Z]+$/;
	return email2.match(emailRegex);

};

Checker.password2 = function() 
{
	const password2 = document.querySelector("#password2");
	return password2.value.length >= 6;
};

Checker.password3 = function() 
{
	const password3 = document.querySelector("#password3");
	const password2 = document.querySelector("#password2");
	/* On rajoute le cas où le premier mot de passe est faux, pour 
	* que l'utilisateur sache que le premier doit être bon pour confirmer */
	return (password2.value.length >= 6 && password3.value == password2.value);
};

Checker.defaultCheck1 = function()
{
	return(document.querySelector("#defaultCheck1").checked);
};

/* eslint-disable */
function passStrength()
{
	const password2 = document.querySelector("#password2");
	/* Ce regex nous permet de vérifier que la chaîne utilise des majuscules, 
	* des miniscules, des numéros, et qu'elle fasse plus de 6 caractères */
	const passwdRegex = /((?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])).{6,}/;

	// Si le mot de passe est inférieur à 6 caractères
	if (password2.value.length < 6)
	{
		return 0;
	}
	// Si le mot de passe match le regex
	else if (password2.value.match(passwdRegex))
	{
		return 2;
	}
	// S'il ne match pas mais est supérieur ou égal à 6 caractères
	else
	{
		return 1;
	}
};