const Feedback = {};

/* OMarche pas, faut plutôt compter nombres éléments validates */
Feedback.validate = function(target) 
{
	target.classList.remove("is-invalid");
	target.classList.add("is-valid");
};

Feedback.invalidate = function(target) 
{
	target.classList.remove("is-valid", "is-invalid");
};

Feedback.error = function(target) 
{
	target.classList.remove("is-valid");
	target.classList.add("is-invalid");
};

// On doit sélectionner tous les éléments du formulaire
document.querySelectorAll("#sign").forEach ( element => 
{
	element.onchange = function(event)
	{
		event.preventDefault();
		let progressBarCount = 0;
		// Pour compter dans le formulaire les classes is-valid
		let arrayValid = document.querySelectorAll("#sign .is-valid");
		arrayValid.forEach ( () => 
		{
			progressBarCount += 100/6;
		});

		// On convertit en float le string
		let tempStyleString = progressBarCount.toString();

		// On lui rajoute un signe pourcentage, que la valeur soit valable pour le CSS
		tempStyleString += "%";

		// On injecte dans le document le nouveau style
		document.querySelector("#progressBarForm").style.width = tempStyleString;
	};
});