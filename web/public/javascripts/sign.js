/* global Checker Feedback passStrength */
document.querySelectorAll("input, select").forEach(el => 
{
	el.onchange = function(event) 
	{
		const target = event.target;
		if (Checker[target.id]()) 
		{
			Feedback.validate(target);
		} 
		else 
		{
			Feedback.invalidate(target);
		}
	};
});

document.querySelector("#password2").onkeyup = function(event)
{
	event.preventDefault();
	if (passStrength()==0) 
	{
		// On doit accéder au premier fils du span, et avoir sa nodeValue pour récupérer le texte
		document.querySelector("#passwordStrengthText").firstChild.nodeValue = "Mot de passe invalide.";

		/* On doit rendre le texte et la barre rouge, en leur changeant la classe success pour danger
		* Par sécurité on retire chaque classe possible, vu que notre mot de passe peut être copié
		* ou la personne peut changer en cours de route. Pour éviter les problèmes de classes on 
		* les retire toute à chaque fois */
		document.querySelector("#passwordStrengthText").classList.remove("text-success");
		document.querySelector("#passwordStrengthText").classList.remove("text-warning");
		document.querySelector("#passwordStrengthText").classList.add("text-danger");
		document.querySelector("#progresspassword").firstElementChild.classList.remove("bg-success");
		document.querySelector("#progresspassword").firstElementChild.classList.remove("bg-warning");
		document.querySelector("#progresspassword").firstElementChild.classList.add("bg-danger");
		
		// On doit retirer la propriété none au texte, en le passant à display:block
		document.querySelector("#passwordStrengthText").style = "display:block";

		/* Display:block pour la barre la rend visible sans son pourcentage, on lui
		* préfère donc visibility, en passant de hidden à visible */
		document.querySelector("#progresspassword").style = "visibility:visible";

		// On prend firstElementChild, et pas le firstChild, car sinon il sélectionne un #text
		document.querySelector("#progresspassword").firstElementChild.style.width = "35%";
	} 
	else if (passStrength()==1)
	{
		document.querySelector("#passwordStrengthText").firstChild.nodeValue = "Force du mot de passe faible.";
		// On doit rendre le texte et la barre jaune, en leur changeant la classe success pour warning
		document.querySelector("#passwordStrengthText").classList.remove("text-danger");
		document.querySelector("#passwordStrengthText").classList.remove("text-success");
		document.querySelector("#passwordStrengthText").classList.add("text-warning");
		document.querySelector("#progresspassword").firstElementChild.classList.remove("bg-danger");
		document.querySelector("#progresspassword").firstElementChild.classList.remove("bg-success");
		document.querySelector("#progresspassword").firstElementChild.classList.add("bg-warning");
		
		// Rendre visible les éléments
		document.querySelector("#passwordStrengthText").style = "display:block";
		document.querySelector("#progresspassword").style = "visibility:visible";

		// On prend firstElementChild, et pas le firstChild, car sinon il sélectionne un #text
		document.querySelector("#progresspassword").firstElementChild.style.width = "65%";
	}
	else
	{
		document.querySelector("#passwordStrengthText").firstChild.nodeValue = "Force du mot de passe excellente.";
		// On doit rendre le texte et la barre vert, en leur changeant la classe warning par success
		document.querySelector("#passwordStrengthText").classList.remove("text-warning");
		document.querySelector("#passwordStrengthText").classList.remove("text-danger");
		document.querySelector("#passwordStrengthText").classList.add("text-success");
		document.querySelector("#progresspassword").firstElementChild.classList.remove("bg-warning");
		document.querySelector("#progresspassword").firstElementChild.classList.remove("bg-danger");
		document.querySelector("#progresspassword").firstElementChild.classList.add("bg-success");
		
		// Rendre visible les éléments
		document.querySelector("#passwordStrengthText").style = "display:block";
		document.querySelector("#progresspassword").style = "visibility:visible";

		// On prend firstElementChild, et pas le firstChild, car sinon il sélectionne un #text
		document.querySelector("#progresspassword").firstElementChild.style.width = "100%";
	}
};

document.querySelector("#sign").onsubmit = function(event) 
{
	event.preventDefault();
	// Vérifier les élements, si un pas valid, les feeback.error
	// document.querySelectorAll(".signForm") permet d'avoir toutes les cases
	// document.querySelectorAll("#sign .is-valid") celles validées
	// Si c'est bon, doit y avoir le même nombres et les mêmes cases
	let arrayValidate = document.querySelectorAll("#sign .is-valid");
	let arrayAll = document.querySelectorAll(".signForm");
	if (arrayValidate.length == arrayAll.length)
	{
		this.submit();
	}
	arrayAll.forEach ( element =>
	{
		if (!(element.classList.contains("is-valid")))
		{
			Feedback.error(element);
		}
	});
};	