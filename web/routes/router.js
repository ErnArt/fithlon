var express = require("express");
var router = express.Router();

const indexController = require("../controllers/indexController");
const userController = require("../controllers/userController");
const dataController = require("../controllers/dataController");
const historiqueController = require("../controllers/historiqueController");

router.get("/", indexController.index);

router.get("/inscription", userController.signUp);
router.post("/inscription", userController.register);

router.post("/connexion", userController.signIn);
router.get("/deconnexion", userController.signOut);

router.post("/sendData", dataController.sendData);

router.get("/historique", historiqueController.historique);

router.get("*", function(req, res){
	res.redirect("/");
});

module.exports = router;